from django.shortcuts import render,redirect,HttpResponse
from django.http import JsonResponse
from django.template.loader import render_to_string
from .models import *
from django.forms import *
from appointment.forms import *
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import View,TemplateView
from django.urls import reverse_lazy
from django.db import transaction
from django.forms.models import inlineformset_factory,modelformset_factory
from datetime import datetime, timedelta
import datetime as dt
from django.contrib.auth.models import User
from rental.models import RentalTransaction,Product




class GeneralSettingView(View):
    def get(self,request):
        sec_form = ScheduleForm()
        user = DoctorProfile.objects.all()
        time = Schedule.objects.all()
        schedules = Schedule.DAY_CHOICE
        # day = Schedule.objects.filter(day='day')
        slots = SlotDuration.objects.all()
        return render(request,'appointment/general_setting.html',{'schedules':schedules,'user':user,'time':time,'slots':slots})
    
    def post(self,request):
        doctor = DoctorProfile.objects.get(doctor = request.user)
        for i in range(len(request.POST.getlist('day'))):
            
            day = request.POST.getlist('day')[i]
            start_time = request.POST.getlist('start_time')[i]
            end_time = request.POST.getlist('end_time')[i]
            slot_duration = request.POST.getlist('slot_duration')[i]
            Schedule.objects.create(doctor=doctor,day=day,start_time=start_time,end_time=end_time,slot_duration_id=slot_duration)
            print(request.POST)
        return redirect('/dashboard')
        
class HolidayView(View):
    def get(self,request):
        holidays = Holiday.objects.all()
        return render(request,'appointment/holiday.html',{'holidays':holidays})

    def post(self,request):
        data = request.POST
        for i in range(len(request.POST.getlist('date_of_holiday'))):
            date_of_holiday = request.POST.getlist('date_of_holiday')[i]
            occassion = request.POST.getlist('occassion')[i]

            Holiday.objects.create(date_of_holiday=date_of_holiday,occassion=occassion)
            print(request.POST)
        return redirect('/')


class CustomSettingView(View):
    def get(self,request):
        slots = SlotDuration.objects.all()
        return render(request,'appointment/custom_setting.html',{'slots':slots})
    
    def post(self,request):
        for i in range(len(request.POST.getlist('select_date'))):
            select_date = request.POST.getlist('select_date')[i]
            start_time = request.POST.getlist('start_time')[i]
            end_time = request.POST.getlist('end_time')[i]
            slot_duration = request.POST.getlist('slot_duration')[i]

            CustomSetting.objects.create(select_date=select_date,start_time=start_time,end_time=end_time,slot_duration_id=slot_duration)
            
        return redirect('/')



class CalendarView(View):
    def get(self,request):
    #     day_values = (
    #         ('Mon', 'Monday')
    #     )

    #     time = request.GET.get('time')
    #     day = time.split(" ")[0]
    #     for x,y in day_values:
    #         if x==day:
    #             full_day =y
    #     time_slot = Schedule.objects.filter(day=full_day)
    #     print(time_slot)
        return render(request,'appointment/booking_page.html',{})


class Ap(View):
    def get(self,request):

        day_values = (
            ('Mon', 'Monday'),
            ('Tue', 'Tuesday'),
            ('Wed', 'Wednesday'),
            ('Thu','Thursday'),
            ('Fri', 'Friday'),
            ('Sat' ,'Saturday'),
            ('Sun' ,'Sunday'),

        )

        time = request.GET.get('time')
        day = time.split(" ")[0]
        for x,y in day_values:
            if x==day:
                full_day = y
        timings = Schedule.objects.filter(day=full_day)
        context = {'timings':timings}
        # print(timings)
        # print(timings[0].start_time)
        # for t in timings:
        #     type(t)
            
        #     # start_time =t.start_time
        #     start_time = datetime.strptime('09:00:00', '%H:%M:%S')
        #     # start_time1 = dt.time(start_time.hour, start_time.minute,  start_time.second)
        #     # end_time  = t.end_time
        #     end_time = datetime.strptime('17:00:00', '%H:%M:%S')
        #     # end_time1 = dt.time(end_time.hour, end_time.minute,  end_time.second)
        #     seq1 = []
        #     while start_time < end_time:
        #         s_t = start_time + datetime.timedelta(minutes = 30)
        #         seq1.append(s_t.strftime("%H:%M:%S"))
        #     context[t.id] = seq1
        #     print(seq1)
        form = render_to_string('appointment/slot_snippet.html',context=context, request=request)
        return JsonResponse({'form': form})


class BookAppointmentView(View):
    def get(self,request):
        user = request.user
        form = CreateAppointmentForm(instance=user)
        return render(request,'appointment/book_appointment.html',{'form':form})
    
    def post(self,request):
        user = request.user
        form = CreateAppointmentForm(instance=user)
        if request.method == 'POST':
            form = CreateAppointmentForm(request.POST or None)
            if form.is_valid():
                form.save()
            return redirect('/')
        # return render(request)


class BookingList(View):
    def get(self,request):
        b_a = BookAppointment.objects.all()
        context = {'b_a':b_a}
        return render(request,'appointment/booking_list.html',context)

# class RentItemView(View):
#     def get(self,request,pk):
#         pdt = Product.objects.get(id=pk)
#         u = pdt.doctorprofile_set.all()
#         s = 'this is good'
#         context = {'pdt':pdt,'u':u,'s':s}
     
#         return HttpResponse(context)


# class GeneralSettingView(View):
#     def get(self,request):
#         day_cat = Dayy.objects.all()
#         return render(request,'appointment/general_setting.html',{'day_cat':day_cat})
    
#     def post(self,request):
#         data = request.POST
#         context = {'data':data,'has_error':False}

#         day = request.POST.get('day')
#         start_time = request.POST.get('start_time')
#         end_time = request.POST.get('end_time')
#         try:
#             if start_time < end_time:
#                 raise "You can not choose this time"
#         except expression as identifier:
#             pass

#         if context['has_error']:
#             return render(request,'appointment/general_setting.html',context)


#         setting = Time.objects.create(start_time=start_time,end_time=end_time, day_id=int(day))
#         return redirect('/')





