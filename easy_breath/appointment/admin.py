from django.contrib import admin

# Register your models here.

from .models import Holiday,DoctorProfile,Schedule,SlotDuration,CustomSetting,BookAppointment


admin.site.register(DoctorProfile)
admin.site.register(Schedule)
admin.site.register(SlotDuration)
admin.site.register(Holiday)
admin.site.register(CustomSetting)
admin.site.register(BookAppointment)

