from django.forms import ModelForm
from django import forms
from appointment.models import Schedule,BookAppointment
from django.forms.models import inlineformset_factory,modelformset_factory

# from crispy_forms.helper import FormHelper
# from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
# from .custom_layout_object import *



class ScheduleForm(ModelForm):
    class Meta:
        model = Schedule
        fields = ['day','start_time','end_time']

        
# class DayyForm(ModelForm):
#     class Meta:
#         model = Dayy
#         fields = ['user','day']


# class TimeForm(ModelForm):
#     class Meta:
#         model = Time
#         fields = ['day','start_time','end_time']

class CreateAppointmentForm(forms.ModelForm):
    class Meta:
        model = BookAppointment
        fields = ['first_name','last_name','email','timing','address','payment_status']
