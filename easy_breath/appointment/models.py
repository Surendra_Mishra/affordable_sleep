from django.db import models
from django.core.exceptions import ValidationError
from multiselectfield import MultiSelectField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class DoctorProfile(models.Model):
    doctor          = models.OneToOneField(User,on_delete=models.CASCADE,null=True,blank=True)
    first_name      = models.CharField(max_length=120,blank=True,null=True)
    last_name       = models.CharField(max_length=120,null=True,blank=True)

    def __str__(self):
        return f'{self.first_name}-{self.last_name}'

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profile Details'


class Schedule(models.Model):
    DAY_CHOICE = (
        ('Monday','Monday'),
        ('Tuesday','Tuesday'),
        ('Wednesday','Wednesday'),
        ('Thursday','Thursday'),
        ('Friday','Friday'),
        ('Saturday','Saturday'),
        ('Sunday','Sunday'),
    )
    doctor          = models.ForeignKey('DoctorProfile',on_delete=models.CASCADE,blank=False,null=True,related_name='schedule')
    day             = models.CharField(max_length=10,choices=DAY_CHOICE,blank=False,null=True)
    start_time      = models.TimeField()
    end_time        = models.TimeField()
    slot_duration   = models.ForeignKey('SlotDuration',on_delete=models.CASCADE,blank=False,null=True)

    def __str__(self):
        return f'{self.day}-{self.start_time}-{self.end_time}-{self.slot_duration}'
    
    def clean(self):
        if self.start_time > self.end_time:
            raise ValidationError('Start should be before end')
        return super().clean()

    class Meta:
        verbose_name        = 'Schedules'
        verbose_name_plural = 'Schedule'


class SlotDuration(models.Model):
    SLOT_DURATION_CHOICE = (
        (1,'15min'),
        (2,'30min'),
        (3,'45min'),
    )
    slot = models.PositiveIntegerField(choices=SLOT_DURATION_CHOICE)

    def __str__(self):
        return f'{self.get_slot_display()}'
    
    class Meta:
        verbose_name        = 'Slot Duration'
        verbose_name_plural = 'Slot Durations'


class Holiday(models.Model):
    date_of_holiday = models.DateField()
    occassion       = models.CharField(max_length=100,blank=True,null=True)

    def __str__(self):
        return f'{self.occassion}'

    class Meta:
        verbose_name = 'Holidays'
        verbose_name_plural = 'Holiday'



class CustomSetting(models.Model):
    select_date     = models.DateTimeField(blank=True,null=False)
    start_time      = models.TimeField()
    end_time        = models.TimeField()
    slot_duration   = models.ForeignKey('SlotDuration',on_delete=models.CASCADE,blank=True,null=False)

    def __str__(self):
        return f"{self.select_date}-{self.start_time}-{self.end_time}-{self.slot_duration}"

    class Meta:
        verbose_name = 'Custom Setting'
        verbose_name_plural = 'Custom Settings'


class BookAppointment(models.Model):
    SLOT_TIMING = (
        (1,'09:00'),
        (2,'09:30'),
        (3,'10:00'),
    )
    patient        = models.OneToOneField(DoctorProfile,on_delete=models.CASCADE,blank=True,null=True,related_name='patient_book')
    timing         = models.IntegerField(choices=SLOT_TIMING)
    first_name     = models.CharField(('First Name'),max_length=50,blank=True,null=True)
    last_name      = models.CharField(('Last Name'),max_length=50,blank=True,null=True)
    email          = models.EmailField()
    address        = models.CharField(max_length=200,blank=True,null=True)
    payment_status = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.first_name}-{self.last_name}-{self.timing}-{self.payment_status}"
    
    class Meta:
        verbose_name = 'Book Appointment'
        verbose_name_plural = 'Book Appointments'
    
# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         BookAppointment.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.bookappointment.save()