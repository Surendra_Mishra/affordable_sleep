from django.urls import include, path,re_path
from .views import GeneralSettingView,HolidayView,CustomSettingView,CalendarView,Ap,BookAppointmentView,BookingList

urlpatterns = [
    path('general-setting/',GeneralSettingView.as_view(),name='general'),
    path('holiday-setting/',HolidayView.as_view(),name='holiday'),
    path('custom-setting/',CustomSettingView.as_view(),name="custom"),
    path('appointment-book/',CalendarView.as_view(),name="app"),  
    path('ap/',Ap.as_view(),name='ap'),
    path('book/',BookAppointmentView.as_view(),name="book"),
    path('booking_list/',BookingList.as_view(),name="booking_list"),
    # path('view_item/<int:pk>/',RentItemView.as_view(),name="view_list"),
]