from django.urls import include, path,re_path
from . import views
# from .views import *

urlpatterns = [
    path('product_list/',views.product_list,name='product_list'),
    path('create_product/',views.create_product,name='create_product'),
    path('update_product/<int:pk>/',views.update_product,name='update_product'),
    path('delete_product/<int:pk>/',views.delete_product,name='delete_product'),
    path('detail_product/<int:pk>/',views.detail_product,name='detail_product'),
    path('rent_item/<int:pk>/',views.rent_product,name='rent_item'),
    path('receive_item/<int:pk>/',views.receive_product,name='receive_item'),
    path('view_rented_item/<int:pk>',views.rent_item_view,name='view_rented_item'),
    path('create_category/',views.create_category,name='create_category'),
    
    

]