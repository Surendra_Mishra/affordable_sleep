from django.contrib import admin
from .forms import ProductCreateForm,IssueForm
# Register your models here.
from .models import Category,Product,RentalTransaction,ReceiveTransaction


class ProductAdmin(admin.ModelAdmin):
    list_display = ['category','name','serial_no','model_no','status']
    form = ProductCreateForm

admin.site.register(Category)
admin.site.register(Product,ProductAdmin)
admin.site.register(RentalTransaction)
admin.site.register(ReceiveTransaction)