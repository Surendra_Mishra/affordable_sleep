from django import forms
from .models import Category,Product,RentalTransaction,ReceiveTransaction

class ProductCreateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['category','name','serial_no','model_no','status']

        def clean_category(self):
            category = self.cleaned_date.get('category')
            if not category:
                raise forms.ValidationError("This field is requied")
            return category

        def clean_name(self):
            name = self.cleaned_date.get('name')
            if not name:
                raise forms.ValidationError('This field is required')
            return name

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['category_name']

class ProductSearchForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['category','name','model_no']

class ProductUpdateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['category','name','serial_no','model_no','status']

class IssueForm(forms.ModelForm):
    class Meta:
        model = RentalTransaction
        fields = ['product','patient_name']


class ReceiveTransactionForm(forms.ModelForm):
    class Meta:
        model = ReceiveTransaction
        fields = ['product','patient_name']
    
  


