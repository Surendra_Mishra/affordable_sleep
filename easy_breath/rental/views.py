from django.shortcuts import render,redirect,get_object_or_404,HttpResponse
from .models import Category , Product,RentalTransaction
from django.views.generic import View,CreateView,TemplateView
from .forms import ProductCreateForm,ProductSearchForm,ProductUpdateForm,IssueForm,ReceiveTransactionForm,CategoryForm
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from appointment.models import DoctorProfile
# Create your views here.



def product_list(request):
    form = ProductSearchForm(request.POST or None)
    products = Product.objects.all()
    context = {'products':products,'form':form}

    if request.method == 'POST':
        category = form['category'].value()
        queryset = Product.objects.filter(
            name__icontains=form['name'].value()
        )
        if (category != ""):
            queryset = queryset.filter(category_id=category)
        context = {'form':form,'queryset':queryset}
    return render(request,'rental/all_products.html',context=context)

def create_category(request):
    form = CategoryForm()
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Categeory added succesfully')
            return redirect('/create_product')
    context = {'form':form}
    return render(request,'rental/create_category.html',context)

def create_product(request):
    form = ProductCreateForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request,"Item added successfully!")
        return redirect('/product_list')
    context={'form':form}
    return render(request,'rental/product_item.html',context=context)


def update_product(request,pk):
    queryset = Product.objects.get(id=pk)
    form = ProductUpdateForm(instance=queryset)
    if request.method == 'POST':
        form = ProductUpdateForm(request.POST,instance=queryset)
        if form.is_valid():
            form.save()
            messages.success(request,"Item updated successfully!")
            return redirect('/product_list')

    context = {'form':form}
    return render(request,'rental/product_item.html',context=context)


def delete_product(request,pk):
    queryset = Product.objects.get(id=pk)
    if request.method == 'POST':
        queryset.delete()
        messages.success(request,"Item deleted successfully!")
        return redirect('/product_list')
    return render(request,'rental/delete_item.html')


def detail_product(request,pk):
    queryset = Product.objects.filter(id=pk)
    context = {'queryset':queryset}
    return render(request,'rental/detail_item.html',context)




# def rent_product(request,pk):
    
#     queryset = Product.objects.get(id=pk)
#     context = {'queryset':queryset}
#     # try:
#     #     ss = RentalTransaction.objects.get(product=queryset)
#     #     context['ss']=ss
#     # except ObjectDoesNotExist:
#     #     pass
        
#     print(queryset)
#     form = IssueForm(instance=queryset)
#     context['form'] = form
#     if request.method == 'POST':
#         # import pdb
#         # pdb.set_trace()
#         form = IssueForm(request.POST,instance=queryset)
#         if form.is_valid():
#             form.save()
#             return redirect('/product_list')
#         print(form.errors)
#     return render(request,'rental/issue_item.html',context)

def rent_product(request,pk):
    queryset = Product.objects.get(id=pk)
    ins_pdt = queryset.rented_product.product
    form = IssueForm(initial = {'product': ins_pdt})
    if request.method == "POST":
        form  = IssueForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/product_list')
            messages.success(request,'item added for rent successfully')
    context = {'form':form,'queryset':queryset}
    return render(request,'rental/issue_item.html',context)

def receive_product(request,pk):
    queryset = Product.objects.get(id=pk)
    context = {'queryset':queryset}
    try:
        ss = RentalTransaction.objects.get(product=queryset)
        context['ss']=ss
    except ObjectDoesNotExist:
        pass
    rented_user = queryset.rented_product.patient_name
    rented_product = queryset.rented_product.product
    form = ReceiveTransactionForm(initial={'product':rented_product,'patient_name':rented_user})
    context['form']=form
    if request.method == 'POST': 
        form = ReceiveTransactionForm(request.POST)
        if form.is_valid():
            form.save()
            ss,created = RentalTransaction.objects.get_or_create(product=queryset)
            print(ss)
            ss.product_name = ss.product
            ss.product = None
            ss.save()   
            messages.success(request,'Item receive successfully')
            return redirect('/product_list')
    
    return render(request,'rental/receive_item.html',context)

def rent_item_view(request,pk):
    obj = Product.objects.get(id=pk)
    # print(obj.rented_product.patient_name)
    rent = obj.rented_product.patient_name
    pdt = obj
    context = {'rent':rent,'pdt':pdt}
    return render(request,'rental/view_issued_item.html',context)




# class ProductView(View):
#     def get(self,request):
#         products = Product.objects.all()
#         context = {'products':products}
#         return render(request,'rental/all_products.html',context=context)   
    


# class CreateCategoryView(View):
#     def get(self,request):
#         cat = Category.objects.all()
#         return render(request,'rental/category.html',{'cat':cat})
    
#     def post(self,request):
#         category_name = request.POST.get('category_name')
#         Category.objects.create(category_name=category_name)
#         return redirect('/')
        
# class CreateProductView(View):
#     def get(self,request):
#         pdt = Product.objects.all()
#         categs = Category.objects.all()
#         statuss = Product.ITEM_QUANTITY_CHOICE
#         context = {'pdt':pdt,'categs':categs,'statuss':statuss}
#         return render(request,'rental/product_item.html',context=context)
    
#     def post(self,request):
#         category    = request.POST.get('category')
#         name        = request.POST.get('name')
#         model_no    = request.POST.get('model_no')
#         quantity    = request.POST.get('quantity')
#         status      = request.POST.get('status')
#         Product.objects.create(category_id=category,name=name,model_no=model_no,quantity=quantity,status=status)
#         print(Product)

#         return redirect('/')

# class ProductUpdateView(View):
#     def get(self,request,pk):
#         product_inst = Product.objects.get(pk=pk)
#         product_inst.category=category
#         product_inst.name=name
#         product_inst.model_no=model_no
#         product_inst.quantity=quantity
#         product_inst.status=status
#         product_inst.save()
#         return redirect('/pdt_list')


#         return render(request,'rental/product_item.html',{'product_inst':product_inst})

   

    # def updateProduct(self,request,pk):
    #     instance = Product.objects.get(category_id=category,pk=pk)
    #     instance.name=name
    #     instance.model_no=model_no
    #     instance.quantity=quantity
    #     instance.status=status
    #     instance.save()
    #     return redirect('/pdt_list')
        

        



