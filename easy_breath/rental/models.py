from django.db import models
from django.db.models import F
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from appointment.models import DoctorProfile
# Create your models here.

class Category(models.Model):
    category_name = models.CharField(max_length=55,blank=False,null=False)

    def __str__(self):
        return f'{self.category_name}'

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

class Product(models.Model):
    ITEM_QUANTITY_CHOICE=(
        ('Rented','Rented'),
        ('Available','Available'),
        ('Maintainance','Maintainance')
    )
    category                  = models.ForeignKey('Category',null=False,blank=True,on_delete=models.CASCADE,related_name='products')
    name                      = models.CharField(max_length=55,blank=True,null=False)
    serial_no                 = models.CharField(max_length=20,blank=True,null=True,unique=True)
    model_no                  = models.CharField(max_length=55,blank=True,null=True)
    # available_quantity        = models.IntegerField(default=0,blank=True,null=True)
    status                    = models.CharField(max_length=13,choices=ITEM_QUANTITY_CHOICE,default='Available')

    def __str__(self):
        return f'{self.category}-{self.name}-{self.serial_no}-{self.status}'

        
    
    class Meta:
        verbose_name = "Product"
        verbose_name_plural = 'Products'



class RentalTransaction(models.Model):
    # issued_quantity        = models.IntegerField(default=0,blank=True,null=True)
    time                   = models.DateTimeField(auto_now=True)
    product                = models.OneToOneField(Product,on_delete=models.CASCADE,blank=True,null=True,related_name='rented_product')
    patient_name           = models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True)
    product_name           = models.ForeignKey('Product',on_delete=models.CASCADE,blank=True,null=True,related_name='product_name')


    def __str__(self):
        return f"{self.product}-{self.patient_name}-{self.time}"
    
    # def save(self,*args,**kwargs):
    #     if not self.pk:
    #         Product.objects.filter(pk=self.product_id).update(available_quantity=F('available_quantity')-self.issued_quantity)
    #     super().save(*args,**kwargs)
    
    def save(self,*args,**kwargs):
        if not self.pk:
            Product.objects.filter(pk=self.product_id).update(status='Rented')
        else:
             Product.objects.filter(pk=self.product_id).update(status='Available')
        super().save(*args,**kwargs)

    
    class Meta:
        verbose_name = 'Rental Transaction'
        verbose_name_plural = 'Rental Transactions'


class ReceiveTransaction(models.Model):
    # received_quantity      = models.IntegerField(default=0,blank=True,null=True)
    time                   = models.DateTimeField(auto_now=True)
    product                = models.ForeignKey('Product',on_delete=models.CASCADE,blank=True,null=True,related_name='receive_product')
    patient_name           = models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True)

    def __str__(self):
        return f"{self.product}-{self.patient_name}-{self.time}"
    
    # def save(self,*args,**kwargs):
    #     if not self.pk:
    #         Product.objects.filter(pk=self.product_id).update(available_quantity=F('available_quantity') + self.received_quantity)
    #     super().save(*args,**kwargs)
    
    def save(self,*args,**kwargs):
        if not self.pk:
            Product.objects.filter(pk=self.product_id).update(status='Available')
        else:
            Product.objects.filter(pk=self.product_id).update(status='Rented')
        super().save(*args,**kwargs)

    class Meta:
        verbose_name = 'Received Transaction'
        verbose_name_plural = 'Received Transactions'
