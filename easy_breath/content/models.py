from django.db import models

from wagtail.core.models import Page,Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel,InlinePanel,MultiFieldPanel
from modelcluster.fields import ParentalKey
from wagtail.images.edit_handlers import ImageChooserPanel

# Create your models here.
# this is the main content models e.g about page 
from blog.models import BlogPage


class MainCarouselImage(Orderable):
    # this is carousel image models
    page = ParentalKey("content.MainPage",related_name = "carousel_images")
    carousel_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL, 
        related_name="+"   
    )

    panels = [
        ImageChooserPanel('carousel_image')
    ]


class MainPage(Page):

    template = 'promotions/home.html'
    heading = models.CharField(max_length=130,null=True,blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('heading'),
        # InlinePanel('carousel_images') 
        MultiFieldPanel([
            InlinePanel('carousel_images',min_num=1,label="Image"),# using the related name here for the inlinepanel to show the carousel image in mainpage

        ],heading="Carousel Images")
    ]
    
    class Meta:
        verbose_name = 'Main Page'
        verbose_name_plural = 'Main Page'



class AboutPage(Page):
    """ Flexible page class """

    template = 'content/about_page.html'

    #todo the stream fields 
    #content = StreamField()

    heading = models.CharField(max_length=130,null=True,blank=True)
    sub_heading = models.CharField(max_length=100,null=True,blank=True)
    text = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel('heading'),
        FieldPanel('sub_heading'),
        FieldPanel('text'),
    ]

    class Meta:
        verbose_name = 'Flex Page'
        verbose_name_plural = 'Flex Pages'

