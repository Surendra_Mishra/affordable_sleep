from django.db import models

from django.shortcuts import render 
from wagtail.admin.edit_handlers import FieldPanel,StreamFieldPanel
from wagtail.core.fields import StreamField,RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel
from streams import blocks
from wagtail.contrib.routable_page.models import RoutablePageMixin,route
# Create your models here.
from wagtail.search import index
from wagtail.core.models import Page



class BlogIndexPage(RoutablePageMixin,Page):
    intro = RichTextField(blank=True)

    def get_context(self,request):
        context = super().get_context(request)
        blogpages = BlogPage.objects.live().public().order_by('-first_published_at')
        context['blogpages'] = blogpages
        return context
    @route(r'^latest/$')
    def latest_blog_post(self,request,*args,**kwargs):
        context = self.get_context(request,*args,**kwargs)
        context['blogpages'] = BlogPage.objects.all().order_by('-date')[:2]
        return render(request,'blog/latest.html',context)
        # return render(request,'promotions/home.html',context)

    content_panels = Page.content_panels + [
        FieldPanel('intro',classname="full")
    ]


class BlogPage(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    blog_image = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name='+',
        on_delete=models.SET_NULL
    )

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('intro'),
        ImageChooserPanel("blog_image"),
        FieldPanel('body', classname="full"),
        
    ]
# class BlogListingPage(Page):

#   ''' Listing page list all the page of the blog details page '''
#   template = 'blog/blog_listing_page.html'
#   custom_title = models.CharField(
#       max_length=255,
#       blank=False,
#       null=False,
#       help_text='Overwrite the default title'
#   )

#   content_panels = Page.content_panels + [
#       FieldPanel("custom_title")
#   ]

#   def get_context(self,request,*args,**kwargs):
#       """ Adding custom stuff to our context """
#       context = super().get_context(request,*args,**kwargs)
#       context['posts']  = BlogDetailPage.objects.live().public()
#       return context



# class BlogDetailPage(Page):
    
#     template = 'blog/blog_detail_page.html'
#     custom_title = models.CharField(
#         max_length=100,
#         null=False,
#         blank=False,
#         help_text='Overwrite the default title'
#     )
    
#     blog_image = models.ForeignKey(
#         "wagtailimages.Image",
#         blank=False,
#         null=True,
#         related_name='+',
#         on_delete=models.SET_NULL
#     )
    
#     content = StreamField(
#         [
#             ("title_and_text",blocks.TitleAndTextBlock()),
#             ("full_richtext",blocks.RichTextBlock()),
#             ("simple_richtext",blocks.SimpleRichTextBlock()),
#             ("cards",blocks.CardBlock()),
#         ],
#         null = True,
#         blank = True
#     )

#     content_panels = Page.content_panels + [
#         FieldPanel("custom_title"),
#         ImageChooserPanel("blog_image"),
#         StreamFieldPanel("content"),
#     ]

