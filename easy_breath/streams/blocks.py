from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock

class TitleAndTextBlock(blocks.StructBlock):
    """ title and text and nothing else"""

    title = blocks.CharBlock(required=True,help_text='Add your title')
    text = blocks.TextBlock(required=True,help_text="Add additional text")

    class Meta:
        template = "streams/title_and_text_block.html"
        icon = "edit"
        label = "Title & Text"


class CardBlock(blocks.StructBlock):
    """Cards with the images and text and buttons"""
    title = blocks.CharBlock(required=True,help_text="Add your title")
    cards = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("image",ImageChooserBlock(required=True)),
                ("title",blocks.CharBlock(required=True,max_length=40)),
                ("text",blocks.TextBlock(required=True,max_length=200)),
                ("button_page",blocks.PageChooserBlock(required=False)),
                ("button_url",blocks.URLBlock(required=False,help_text="for external page link")),

            ]
        )
    )

    class Meta:
        template = 'streams/card_block.html'
        icon = 'placeholder'
        label = 'staff cards'


class RichTextBlock(blocks.RichTextBlock):
    """ Rich text with all the features """

    class Meta:
        template = 'streams/richtext_block.html'
        icon = 'doc-full'
        label = 'Full RichText'


class SimpleRichTextBlock(blocks.RichTextBlock):
    """ Rich text without all the features """
    def __init__(self, required=True,help_text=None,editor='default',features=None,**kwargs):
        super().__init__(**kwargs)
        self.features = [
            "bold",
            "italic",
            "link",
        ]

    class Meta:
        template = 'streams/richtext_block.html'
        icon = 'edit'
        label = 'Simple RichText'
        